import { GOOGLE_API_KEY, DATA_URL } from './_globals.js'

const fetchData = async () => {
  let response = await fetch(DATA_URL)
  let data = await response.json();
  // only proceed once second promise is resolved
  return data.response.data;
}

export default fetchData
