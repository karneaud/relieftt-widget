export const DEFAULT_ZOOM = 15
export const DEFAULT_LNG = -61.266413
export const DEFAULT_LAT = 10.387634
export const GOOGLE_API_KEY = null // to set
export const DATA_URL = './data/sample.json' // to change

let a = {}

Array.from(document.querySelectorAll('script[type="text/template"]')).forEach((c,i,arr) => {
  a[c.id] = c.innerHTML
})

export const templates = a
