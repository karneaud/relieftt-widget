import { templates } from './_globals.js'

export const populateTemplate = function(data, template ) {
  let keys = Object.keys(data),
    regex = new RegExp(keys.map((a, c, i, arr) => '\{\{'.concat(a,'\}\}')).join('|'),'gm')
  return template.replace(regex, (m, o, s) => {
    let key = m.replace(/[\W]+/g,'')
    return data[key]
  })
}

export const sortAlphabetically = (a, b) => {
  if (a.dropOffLocationName < b.dropOffLocationName)
    return -1;
  if (a.dropOffLocationName > b.dropOffLocationName)
    return 1;
  return 0;
}

export const displayData = (data) => {
  let listTempl = templates.listTemplate, html = '', contactHtml = '', descriptionHtml = '', linkHtml = '', timeHtml = ''

  data.forEach((item, i) => {
    html += populateTemplate(item, listTempl)
    if(item.description)
      descriptionHtml = populateTemplate({ description: item.description || '' }, templates.descriptionTemplate)

    if(item.phone1)
      contactHtml = populateTemplate({'contactName': item.contactName1 || 'N/A', 'phone': item.phone1}, templates.contactTemplate)

    if(item.phone2)
      contactHtml += populateTemplate({'contactName': item.contactName2 || 'N/A', 'phone': item.phone2}, templates.contactTemplate)

    if(item.timeOpen)
      {
        timeHtml = populateTemplate({ timeOpen: item.timeOpen, timeOpenFormatted: item.timeOpen, timeClosed: item.timeClosed || '', timeClosedFormatted: item.timeClosed || '' }, templates.timeTemplate )

      }

    if(item.email)
      linkHtml = populateTemplate(
        {
          type: 'email',
          icon: 'fa fa-envelope',
          url : 'mailto:' + item.email,
          link : item.email
        }, templates.linkTemplate)

        if(item.website)
          linkHtml += populateTemplate(
            {
              icon: 'fa fa-globe',
              type: 'website',
              url : item.website,
              link : item.website
            }, templates.linkTemplate)
            if(item.facebook)
              linkHtml += populateTemplate(
                {
                  type: 'facebook',
                  icon: 'fab fa-facebook',
                  url : item.facebook,
                  link : item.facebook
                }, templates.linkTemplate)
    html = populateTemplate( { 'contactstemplate' : contactHtml, 'descriptiontemplate': descriptionHtml, 'linktemplate': linkHtml , 'timetemplate' : timeHtml }, html)
    linkHtml = contactHtml = timeHtml = ''
  })

  return html
}

export const searchCenters = (data, value) => {
  if(value.length < 3 || !(/[\w]+/ig.test(value))) return false

  let results = data.filter((item) => {
    return (new RegExp(value,'ig')).test(item.dropOffLocationName.concat(' ',item.timeOpen,' ',item.description,' ',item.fullAddress,' ',item.cardinalPoint))
  })
  if(results.length < 1) return false

  return results
}
