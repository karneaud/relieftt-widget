import addMarkers from './_map.js'
import fetchData from './_fetch.js'
import { displayData, populateTemplate, searchCenters, sortAlphabetically } from './_functions.js'

var dataSet, list = document.getElementById('listHolder'), searchInput = document.getElementById('relief-search'), clearBtn = document.getElementById('search-clear')

searchInput.addEventListener('keyup', ev => {
  let results = searchCenters( dataSet, ev.target.value )
  if(!results) return

  list.innerHTML = displayData(results)
  addMarkers(results)

})
clearBtn.addEventListener('click', (e) => {
  searchInput.value = ''
  list.innerHTML = displayData(dataSet)
})

fetchData().then(data => {
  dataSet = data.sort(sortAlphabetically)
  list.innerHTML = displayData(data)
  addMarkers(data)
}).catch(err => console.log(err))
