import Map from 'ol/Map';
import View from 'ol/View';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import Vector from 'ol/source/Vector';
import Overlay from 'ol/Overlay';
import VectorLayer from 'ol/layer/Vector';
import { OSM } from 'ol/source.js';
import { fromLonLat } from 'ol/proj';
import TileLayer from 'ol/layer/Tile';
import XYZSource from 'ol/source/XYZ';
import {Icon, Style} from 'ol/style.js';
import { DEFAULT_LAT, DEFAULT_LNG, DEFAULT_ZOOM } from './_globals.js'
import { displayData } from './_functions.js'

const icon = new Style({
  image: new Icon({
    anchor: [0.5, 46],
    anchorXUnits: 'fraction',
    anchorYUnits: 'pixels',
    src: '//upload.wikimedia.org/wikipedia/commons/thumb/9/93/Map_marker_font_awesome.svg/34px-Map_marker_font_awesome.svg.png'
  })
})

const addMarkers = (data) => {
  data.forEach((item, i) => {
    let { latitude, longitude } = item,  marker = new Feature({
        geometry: new Point(
          fromLonLat([longitude, latitude])
        ),

      })
    marker.setStyle(icon)
    marker.values_.data = item
    let vectorSource = new Vector({
        features: [marker]
      }), markerVectorLayer = new VectorLayer({
        source: vectorSource,
        updateWhileAnimating: true,
        updateWhileInteracting: true,
        style: function(feature, resolution) {
          icon.getImage().setScale(map.getView().getResolutionForZoom(3) / resolution);
          return icon;
        }
      })

      map.addLayer(markerVectorLayer);
  })
}, goToMap = (lat, lng, title) => {
  let coords = fromLonLat([lng, lat])
  showPopUp(coords,`<center><h4 class="p-4"><strong>${title}</strong></h4></center>`)
  return false;
}, showPopUp = (coord, html) => {
  content.innerHTML = html
  // Offset the popup so it points at the middle of the marker not the tip
  popup.setOffset([0, -22])
  popup.setPosition(coord)
}

var osmSource = new OSM();

var element = document.getElementById('popup')
, content = document.getElementById('popup-content')
, closer = element.querySelector('#popup-closer')
, popup = new Overlay({
  element: element,
  positioning: 'bottom-center',
  stopEvent: false,
  autoPan: true,
  autoPanAnimation: {
    duration: 250
  }
})


const map = new Map({
  target: 'map',
  layers: [
    new TileLayer({
      source: osmSource
    })
  ],
  view: new View({
    center: fromLonLat([ DEFAULT_LNG, DEFAULT_LAT ]),
    zoom: 8
  })
});

closer.onclick = function() {

       popup.setPosition(undefined)
       closer.blur()
       content.innerHTML = ''
       return false
     };

map.addOverlay(popup)
map.on('singleclick', function(evt) {


  var feature = map.forEachFeatureAtPixel(evt.pixel, function(feature, layer) {
    return feature;
  });

  if (feature) {
    var coord = feature.getGeometry().getCoordinates()
    var props = feature.getProperties()
    showPopUp(coord, displayData([props.data]))
  } else closer.click()
})

window.goToMap = goToMap

navigator.geolocation.getCurrentPosition((pos) => {
  let coords = fromLonLat([pos.coords.longitude, pos.coords.latitude]);
  map.getView().setCenter (coords, DEFAULT_ZOOM);
});

export default addMarkers
