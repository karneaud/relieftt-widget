const webpack = require('webpack'), BrowserSyncPlugin = require('browser-sync-webpack-plugin'), isProduction = process.env.NODE_ENV === 'production', fs = require('fs');

module.exports = {
  entry: [
    'webpack-dev-server/client?https://0.0.0.0:3100/',
    './src/main.js'
  ],
  output: {
    path: __dirname,
    publicPath: '/',
    filename: './build/bundle.js'
  },
  devServer: {
    contentBase: __dirname,
    compress: true,
    port: 3100,
    host: '0.0.0.0',
    disableHostCheck: true,
    https: {
      key: fs.readFileSync('./private.key'),
      cert: fs.readFileSync('./private.crt'),
      ca:  fs.readFileSync('./private.pem')
    },
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    }
  },
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000,
    ignored: /node_modules/
  },
  watch: isProduction,
  plugins:
    !isProduction ?
    [ new BrowserSyncPlugin({
        // browse to http://localhost:3000/ during development
        host: '0.0.0.0',
        port: 3000,
        https: true,
        injectChanges: true,
			  ghostMode: false,
        // proxy the Webpack Dev Server endpoint
        // (which should be serving on http://localhost:3100/)
        // through BrowserSync
        proxy: 'https://0.0.0.0:3100/',
        files: ['./css/style.css', './index.html']
      },
      // plugin options
      {
        // prevent BrowserSync from reloading the page
        // and let Webpack Dev Server take care of this
        reload: false
      })
  ] : []
}
